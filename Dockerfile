FROM python:alpine

WORKDIR /app
ENV CONFIG="/download/config.json"

COPY run.py requirements.txt ./

RUN set -x && \
 apk add --no-cache ffmpeg && \
 pip install -r requirements.txt
RUN youtube-dl --version

ENTRYPOINT ["python"]
CMD ["run.py"]
