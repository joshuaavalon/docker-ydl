from json import dump, load
from os import environ, remove, rename
from pathlib import Path
from subprocess import call

from youtube_dl import YoutubeDL


def process_hook(d):
    if d["status"] == "finished":
        handle(Path(d["filename"]))


def handle(path: Path):
    temp = path.parent.joinpath(Path(path.stem + ".tmp" + path.suffix))
    command = ["ffmpeg", "-i", str(path.absolute()),
               "-map_metadata", "-1",
               "-metadata:s:a", f"language={language}",
               "-metadata:s:v", f"language={language}",
               "-codec", "copy",
               str(temp.absolute())]
    call(command)
    remove(path)
    rename(temp, path)


with open(environ["CONFIG"], "r", encoding="utf-8") as f:
    option = load(f)

urls = option.pop("urls", [])
language = option.pop("language", "eng")

option["progress_hooks"] = [process_hook]

if isinstance(urls, str):
    urls = [urls]

with YoutubeDL(option) as ydl:
    ydl.download(urls)

for j in Path("/download").glob("*.json"):
    with open(j, "r", encoding="utf-8") as f:
        data = load(f)
    with open(j, "w", encoding="utf-8") as f:
        dump(data, f, ensure_ascii=False, indent=2)
